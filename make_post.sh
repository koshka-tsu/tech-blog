#!/bin/bash

fname="hoge.md"
if [ $# -eq 1 ]
  then fname="$1"
fi

cp archetypes/default.md "content/blog/$fname"

code "content/blog/$fname"