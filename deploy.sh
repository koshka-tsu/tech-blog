#!/bin/sh

# Add changes to git.
git add -A

# Commit changes.
msg="rebuilding site `date`"
if [ $# -eq 1 ]
  then msg="$1"
fi
git commit -m "$msg"

# Push source and build repos.
git push origin master

# copy files in blog dir to the obsidian dir
# source_dir=./content/blog/
# target_dir=~/myworld/blog/
# rsync -rv --ignore-existing --exclude='_index.md' --exclude='search.md' $source_dir $target_dir