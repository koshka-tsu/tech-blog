import os
import sys
import re
import time
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import pyautogui
import pyperclip


def get_url(file_name, article_dir):
    """
    mdファイルの参照タグ配下のurlを取得
    """
    file_path = article_dir + file_name
    with open(file_path, 'r', encoding='utf-8') as f:
        md = f.read()

    ref_id = md.index('参照')  # 参照以外のurlは無視
    ref_str = md[ref_id:]
    url_ls = re.findall(r'\((http.*)\)', ref_str)
    # ブラックリスト形式でpdfとyoutubeリンクを無視する
    url_ls = [x for x in url_ls if (x[-3:] != 'pdf') and ('youtube' not in x)]

    return url_ls


def save_mhtml(prefs, url, download_dir):
    """
    webページをmhtml形式でダウンロードする
    参照: https://hk29.hatenablog.jp/entry/2020/04/30/224711
    """

    # ドライバの準備
    options = Options()
    options.add_experimental_option('prefs', prefs)
    driver = webdriver.Chrome(r'C:\Program Files (x86)\chromedriver_win32\chromedriver.exe', options=options)
    driver.set_window_size(1024,768)
    driver.set_window_position(0,0)
    driver.get(url)
    time.sleep(5)

    # 保存ウィンドウを立ち上げる
    pyautogui.hotkey('ctrl', 's')
    time.sleep(1)
    pyautogui.hotkey('Home')
    time.sleep(1)

    # ダウンロード先の指定(Optionの設定ではうまくいかなかったので)
    pyperclip.copy(download_dir)  
    pyautogui.hotkey('ctrl', 'v')
    time.sleep(1)
    pyautogui.typewrite('\\')

    # 保存方法(mhtml)の選択
    pyautogui.hotkey('tab')
    time.sleep(1)
    pyautogui.press('down')
    time.sleep(1)
    pyautogui.press('up')
    pyautogui.press('up')
    pyautogui.press('down')
    time.sleep(1)
    pyautogui.hotkey('enter')
    time.sleep(1)

    # 保存
    pyautogui.hotkey('tab')
    pyautogui.hotkey('enter')
    time.sleep(8)

    # 上書きするか確認されたら
    pyautogui.hotkey('n')
    pyautogui.hotkey('esc')
    driver.close()


def main(file_name, article_dir='./content/blog/', download_dir=r'D:\Htmls'):
    """
    ブログ記事（mdファイル）の参照に記載されたwebサイトをmhtml形式でダウンロードする

    Parameters
    ----------
    file_name: str
        ブログ記事のファイル名
    article_dir: str
        ブログ記事が格納されているディレクトリ
    download_dir: str
        mhtmlの格納先ディレクトリ
    """
    prefs = {}
    prefs['download.prompt_for_download'] = False
    # prefs['download.default_directory'] = download_dir
    prefs['download.directory_upgrade'] = True
    prefs['safebrowsing.enabled'] = True

    url_ls = get_url(file_name, article_dir)

    for i, url in enumerate(url_ls):
        save_mhtml(prefs, url, download_dir)


if __name__ == '__main__':
    args = sys.argv
    if len(args) == 1 or len(args) > 4:
        print('Arguments are not right')

    main(*args[1:])
