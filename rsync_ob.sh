#!/bin/sh

# copy files in blog dir to the obsidian dir
source_dir=./content/blog/
target_dir=~/myworld/blog/
rsync -rv --ignore-existing --exclude='_index.md' --exclude='search.md' $source_dir $target_dir