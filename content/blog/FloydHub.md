---
title: "FloydHub"
date: 2021-03-29T00:06:39+09:00
draft: false
---

# Google Colab Proでの開発に慣れないので，FloydHubに移行した話

Google Colab Proを登録して色々と遊んではみたが，ipynb形式に慣れないのと，セッションの問題などが面倒で[FloydHub](https://www.floydhub.com/)に移行することにした．

## FloydHubとは 
データサイエンティスト用の廉価なクラウドサービス + ホスティングサービス．無料版もあるが学習時間の時間制限などの制約が厳しい．有料版でも9ドル/月と安いため，無料版を使ってみて良かったら有料版に移行したい．無料版(Beginner)と有料版(Data Scientist)の細かい差異(の一部分)は以下のとおり．

![floyd.PNG](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/300536/ca594c98-5cf6-2e4c-8235-6b262595c670.png)

## よいところ
- 環境構築の手間がかからない
- ローカルで書いたコードをCLIでサクッとGPU環境に流せる

## 登録
普通にSign Upすればよいが，無料版でもクレジットカードの入力が求められた．
何もしていしないと無料版での登録となり，登録後に移動するページで"Upgrade your plan"に従うと有料版に移行できる．

## 利用法
実際にコードを動かしてみる．基本的には，[Quick Start](https://docs.floydhub.com/getstarted/quick_start/)に沿えばよい．

**手順:**
1). `floyd-cli`をローカルマシンにインストール
    - `pip install -U floyd-cli`
        - 私の場合，Python 3.6にしないとうまくいかなかった
2). `floyd login` でログイン
    - PowerShell等でこのコマンドを打つと，ブラウザが起動しログインできる
3). プロジェクトの作成
ブラウザでFloydHubに入り，プロジェクト("test"と命名)を作成する．ほぼGitHub. 
![floyd_project.PNG](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/300536/66213967-eeb1-731e-fa7b-e9c9a100abe0.png)

4). "quick-start repository"を動かしてみる

サンプルリポジトリを落とす:

``` sh
$ git clone https://github.com/floydhub/quick-start.git
Cloning into 'quick-start'...
...
$ cd quick-start
$ ls
eval.py  LICENSE  mnist_cnn.ipynb  README.md  train_and_eval.py  train.py
```

先ほど作ったプロジェクトの初期化:

```sh
$ floyd init test

Project "test" initialized in current directory
```

実行:

``` sh
floyd run --gpu --env tensorflow-1.3 "python train_and_eval.py"
```

ジョブをリアルタイムで確認:

```sh
floyd logs <ジョブ名> -f
```

出力:

``` sh
2021-03-28 09:04:13,348 INFO - Iter 192000, Minibatch Loss= 331.912109, Training Accuracy= 0.96094
2021-03-28 09:04:13,509 INFO - Iter 193280, Minibatch Loss= 83.669022, Training Accuracy= 0.98438
2021-03-28 09:04:13,671 INFO - Iter 194560, Minibatch Loss= 320.909058, Training Accuracy= 0.98438
2021-03-28 09:04:13,832 INFO - Iter 195840, Minibatch Loss= 234.908890, Training Accuracy= 0.96875
2021-03-28 09:04:13,993 INFO - Iter 197120, Minibatch Loss= 116.310966, Training Accuracy= 0.96875
2021-03-28 09:04:14,154 INFO - Iter 198400, Minibatch Loss= 54.050278, Training Accuracy= 0.97656
2021-03-28 09:04:14,315 INFO - Iter 199680, Minibatch Loss= 70.160957, Training Accuracy= 0.96094
2021-03-28 09:04:14,346 INFO - Optimization Finished!
2021-03-28 09:04:14,384 INFO - Testing Accuracy: 0.96875
2021-03-28 09:04:15,150 INFO - 
################################################################################

2021-03-28 09:04:15,150 INFO - Waiting for container to complete...
2021-03-28 09:04:15,435 INFO - Persisting outputs...
2021-03-28 09:04:15,787 INFO - Creating data module for output...
2021-03-28 09:04:15,830 INFO - Data module created for output.
2021-03-28 09:04:15,831 INFO - Persisting data in home...
2021-03-28 09:04:16,085 INFO - Home data persisted.
2021-03-28 09:04:16,086 INFO - [success] Finished execution
```

簡単．結果はブラウザ上でも確認できる．

## 各種フレームワークへの対応
上のコマンド`floyd run --gpu --env tensorflow-1.3 "python train_and_eval.py"` にある`--env`オプションには，TensorFlowやPyTorch等のフレームワークを指定することができ，バージョンもほぼ揃っているため自前で環境構築をする必要はない．**ここが一番うれしい**．

---

他にも，JupyterLabをベースにしたIDEをブラウザ上で使えたりするので，ご興味がある方は[Core Concepts](https://docs.floydhub.com/getstarted/core_concepts/)を参照していただきたい．

## 個人的備忘録
手順:
- Windows TerminalでPowerShellを開く
- `cd floyd`
- `pipenv shell`
- `floyd <hoge>` 