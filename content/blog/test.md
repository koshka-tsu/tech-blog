---
title: "Test"
date: 2021-03-17T10:24:37+09:00
draft: false
---

# 備忘録
``````
---
marp: true
theme: default
header: "<header>"
footer: "<footer>"

size: 16:9
paginate: true

style: |
    .slide {
        font-family:'メイリオ', 'Meiryo';
    }

    section.title {
        justify-content: center;
        text-align: center;
    }

    section {
        justify-content: start;
    }

    header {
        color: white;
        width: 100%;
        padding: 5px;
        background: linear-gradient(#5876aa, #5876aa);
        top: 0px;
        left: 0px;
    }  

    footer {
        color: slategray;
    }

---

<!-- 
_class: title
_header: ""
-->
# Marpによる「md -> pptx 自動変換」（n番煎じ）
### SubTitle

<!--
_color: white
_footer: "[Susann Mielke](https://pixabay.com/ja/users/susannp4-1777190/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=2083492)によるPixabayからの画像"
-->
![bg brightness:0.4](cat-2083492_1920.jpg)

---
# はじめに
## 動機
- 内部向けのパワポを手抜きで作りたい
    - 慣れているマークダウンから自動変換したい
    - 勉強会のためにサクッと数式がかけるとうれしい 
- 手を抜くとはいえ、最低限の体裁は整えたい

## 対応
- 「VS Code + Marp」をとりあえず採用（本資料もMarpで作成）
    - 導入方法は割愛

---
# 基本的な書き方
おおむね通常のMarkdown記法に準拠
``` 
---  (<- スライドの区切りを表す)

<!-- _header: "<ヘッダー>" -->

# スライドタイトル
## サブ項目1
- hoge

## サブ項目2
1. fuga

<!-- _footer: "<フッター>" -->
```

---
<!--
_header: ""
-->
# 画像
画像も挿入できる

![bg left:40% brightness:1](./cat-1940487_1280.jpg)

<br>

### mdでの記述
```
![bg left:40% brightness:1](./cat-1940487_1280.jpg)
```

<!--
_footer: '[Karsten Paulick](https://pixabay.com/ja/users/kapa65-61253/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1940487)によるPixabayからの画像'
-->

---
# 数式
$$
\displaystyle \zeta(s) = 1 + \frac{1}{2^s} + \frac{1}{3^s} + \frac{1}{4^s} + \cdots
$$

<br>

### mdでの記述
VS CodeはKaTeXなるレンダリングエンジンをサポートしているので、普通に数式を書くことができる
```
$$
\displaystyle \zeta(s) = 1 + \frac{1}{2^s} + \frac{1}{3^s} + \frac{1}{4^s} + \cdots
$$
```

---
# コードブロック
コードのシンタックスハイライトも可能

``` python
import numpy as np

a = np.array([2,3,4])
print(a.dtype)
```
### mdでの記述
````
``` python
import numpy as np

a = np.array([2,3,4])
print(a.dtype)
```
````

---
# 参考
1. [【VS Code + Marp】Markdownから爆速・自由自在なデザインで、プレゼンスライドを作る](https://qiita.com/tomo_makes/items/aafae4021986553ae1d8)
2. [（備忘録）Marpテンプレート](https://qiita.com/zono_0/items/e6ab64f381440578ea1c)