---
title: "20210209"
date: 2021-02-09T03:05:28+09:00
draft: false
---

# 備忘録

## 深層学習環境構築（PyTorch編）
### PyTorch導入
前回、TensorFlow(Keras)の動作確認を行ったので、今回はPyTorchをインストールする。
1. [PyTorchの公式サイト](https://pytorch.org/)で、OSやCUDAのバージョンに合わせたコマンドを調べる
    - 私の場合、WindowsかつCUDA10.1を選択する
     ![obsidian](https://koshka-tsu.gitlab.io/tech-blog/images/pytorch.png)
    - 下記コマンドを作業ディレクトリで実行
        - `pipenv install torch==1.7.1+cu101 torchvision==0.8.2+cu101 torchaudio===0.7.2 -f https://download.pytorch.org/whl/torch_stable.html`

エラー発生
```
Installing torch==1.7.1+cu101...
Error:  An error occurred while installing torch==1.7.1+cu101!
Error text:
ERROR: Could not find a version that satisfies the requirement torch==1.7.1+cu101
ERROR: No matching distribution found for torch==1.7.1+cu101

Installation Failed
```
ググったらPython3.9なのが悪いっぽいので、3.8にダウングレードする
`pipenv --python 3.8`

だがうまくいかない。requirements.txtを作成してからpip installするとよいらしいので試してみたが、これもダメ。
原因は、pipenvに"-f"オプションがないことだった。pipenvでも直接whlファイルを指定すればダウンロードできるっぽい[^footnote_1]

[先のURL](https://download.pytorch.org/whl/torch_stable.html)に飛んで、ほしいものを探す
- torch: https://download.pytorch.org/whl/cu101/torch-1.7.1%2Bcu101-cp38-cp38-win_amd64.whl
- torchvision: https://download.pytorch.org/whl/cu101/torchvision-0.8.2%2Bcu101-cp38-cp38-win_amd64.whl
- torchaudio: https://download.pytorch.org/whl/torchaudio-0.7.2-cp38-none-win_amd64.whl

あとは、`pipenv instal [URL]`で一つずつ入れる（これは人間がやるべき作業か？）

面倒なので、以下のようにする。

1. `pipenv shell`
2. `pip install torch==1.7.1+cu101 torchvision==0.8.2+cu101 torchaudio===0.7.2 -f https://download.pytorch.org/whl/torch_stable.html`

### テスト
以下のコードをコマンドプロンプトで実行
```
import torch
print(torch.cuda.is_available())
print(torch.cuda.get_device_name())
```
True
GPU名
が帰ってくればok

## 参照
[^footnote_1]: [[PyTorch] Windows10 + Pipenv で PyTorch(GPU)を導入する](https://qiita.com/Haaamaaaaa/items/69b30d1dbbfcea834a9d)