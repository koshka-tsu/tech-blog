---
title: "20210227"
date: 2021-02-27T14:42:24+09:00
draft: false
math: true
---

# 備忘録

## AtCoder
[ABC077 C-Snuke Festival](https://atcoder.jp/contests/abc077/tasks/arc084_a)

### 問題の概略
$A < B < C$なる組み合わせは何通りあるか

### 典型
真ん中($B$)を固定して考える




![obsidian](https://koshka-tsu.gitlab.io/tech-blog/images/obsidian.png)
## 参照
[^footnote_1]: [Windows環境にHugo Extendedをインストールする](https://sanpobiyori.info/20190503/)